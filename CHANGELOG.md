# Changelog

## [1.6.2] - 2021-08-31

### Adicionado
 - MARKET-803: Client-go consegue assinar books dos papéis.

### Alterado
 - MARKET-781: Corrige assinatura de grupos em tio com sinal de 15 minutos.

### Removido


